-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2018 at 02:57 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL,
  `contact_firstname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_middlename` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_lastname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `customer_email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `customer_phone` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `customer_city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customer_zip` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `customer_logofile` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `customer_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_chargelimit` int(11) NOT NULL DEFAULT '0' COMMENT 'Sett til negativt beløp for å tillate overtrekking',
  `customer_balance` int(11) NOT NULL,
  `customer_price` int(11) NOT NULL DEFAULT '2',
  `customer_regcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_email`, `customer_phone`, `customer_city`, `customer_zip`, `customer_address`, `customer_address2`, `customer_logofile`, `customer_notes`, `customer_chargelimit`, `customer_balance`, `customer_price`, `customer_regcode`) VALUES
(1, 'Ultor', 'mail@ultor.com', '', '', '', '', '', '', '', 0, 1000000000, 0, ''),
(2, 'Bobs badeender', 'mail@dudleif.no', '1234', '', '', '', '', '', '', 0, 8, 2, ''),
(5, 'Arnes apekatter', 'ape@apekatter.no', '1234', 'Dyreparken', '99', 'Apeveien 123', '', '', '', -3, -2147483648, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `group_name` varchar(49) COLLATE utf8_unicode_ci NOT NULL,
  `group_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `customer_id`, `group_name`, `group_description`) VALUES
(1, 1, 'Test', 'Dette er en test');

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE `group_members` (
  `member_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `member_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `member_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `group_members`
--

INSERT INTO `group_members` (`member_id`, `group_id`, `member_number`, `member_name`) VALUES
(1, 1, '40607849', 'Samuel');

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `queue_id` bigint(20) NOT NULL,
  `message_id` varchar(42) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID fra API',
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `queue_from` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `queue_to` int(11) NOT NULL,
  `queued_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `queue_message` text COLLATE utf8_unicode_ci NOT NULL,
  `queue_sent` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `queue`
--

INSERT INTO `queue` (`queue_id`, `message_id`, `user_id`, `customer_id`, `queue_from`, `queue_to`, `queued_time`, `queue_message`, `queue_sent`) VALUES
(1, '', 5, 0, '', 0, '2018-05-24 12:54:55', '1234', 0),
(2, '', 1, 1, '', 0, '2018-05-24 12:57:33', '1234', 0),
(3, '', 5, 5, '', 0, '2018-05-24 13:08:12', 'test', 0),
(4, '', 1, 1, '', 0, '2018-05-24 13:19:49', '1234', 0),
(5, '', 1, 2, '', 0, '2018-05-24 13:20:28', '1234', 0),
(6, '', 1, 1, '', 1234, '2018-05-30 10:58:15', 'test', 0),
(7, '', 1, 1, '', 3456, '2018-05-30 10:58:15', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `regcode`
--

CREATE TABLE `regcode` (
  `regcode_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `regcode_code` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regcode`
--

INSERT INTO `regcode` (`regcode_id`, `customer_id`, `permission`, `regcode_code`) VALUES
(1, 1, 2, 'smultring'),
(2, 2, 1, 'bobross'),
(11, 5, 3, '66195493');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `customer_id`, `transaction_date`, `transaction_amount`) VALUES
(1, 1, '2018-05-24 13:19:49', 0),
(2, 2, '2018-05-24 13:20:28', -2),
(3, 1, '2018-05-30 10:58:15', 0),
(4, 5, '2018-05-30 11:07:41', -2147483648);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `user_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `user_registered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_changed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_siteadmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_enabled`, `user_registered`, `user_last_changed`, `user_phone`, `user_siteadmin`) VALUES
(1, 'samuel', 'samuel@artige.no', '$2y$10$Lqrur3ggNjhfzolHpjP/Uul4g49n3uEKX6JEz73aD7/yd38nDgady', 1, '2018-05-09 14:22:00', '2018-05-16 13:08:47', '40607849', 1),
(3, 'bob', 'bob@dudleif.no', '$2y$10$mlACKzbWpe.gU0ZOY0mNUe7ZoQGjHyG1MM5bEaa7g8p1DwqDzNLBe', 1, '2018-05-23 12:51:18', '2018-05-23 10:52:12', NULL, 0),
(5, 'Arne', 'arne@apekatter.no', '$2y$10$KAS0h6snzdN9wkHM3gne8eJrJToOeany0iHG8qkcZzXKdXYR.ylbi', 1, '2018-05-24 14:00:19', '2018-05-30 11:02:59', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_relations`
--

CREATE TABLE `user_relations` (
  `relation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `relation_privilege` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_relations`
--

INSERT INTO `user_relations` (`relation_id`, `user_id`, `customer_id`, `relation_privilege`) VALUES
(1, 1, 1, 3),
(3, 3, 1, 2),
(4, 1, 2, 3),
(5, 3, 2, 3),
(6, 4, 4, 3),
(7, 5, 5, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `customer_name` (`customer_name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`queue_id`);

--
-- Indexes for table `regcode`
--
ALTER TABLE `regcode`
  ADD PRIMARY KEY (`regcode_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_relations`
--
ALTER TABLE `user_relations`
  ADD PRIMARY KEY (`relation_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `queue_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `regcode`
--
ALTER TABLE `regcode`
  MODIFY `regcode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_relations`
--
ALTER TABLE `user_relations`
  MODIFY `relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
