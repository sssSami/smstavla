<?php
if(isset($enforce_auth)) {
	if(!isset($_SESSION['loggedin'])) {
		header('Location: login.php');
		die('<a href="login.php">Logg inn</a>');
	}
}
if(isset($output_mode)) {
	switch($output_mode) { // Hmm, what content type are we serving?
		case 'json':
			header('Content-Type: application/json');
			break;
		case 'xml':
			header('Content-Type: text/xml;Charset=UTF8');
			break;
		case 'html':
		default:
			header('Content-type: text/html;Charset=UTF8');
			break;
	}
} else die(); // Don't let the users access this file directly

if(isset($db_con)) {
	$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	try {
		if(!$db->set_charset('utf8'));
	}
	catch(Exception $e) {
		error_log('SMSTavla: '.$e);
	}
}

$ranks = array(
	0 => 'Ingen tilgang',
	1 => 'Kun sende meldinger',
	2 => 'Regnskap + sende meldinger',
	3 => 'Full administratortilgang'
); // Hardkodede tilganger for å spare tid
