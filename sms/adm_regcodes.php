<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
$db_con = true;
$pagetitle = 'Registreringskoder';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');

if(!empty($_POST)) {
	if(isset($_GET['id'])) $customer = $_SESSION['customers'][intval($_GET['id'])];
	else $customer = $_SESSION['customers'][0];
	$cid = intval($customer['customer_id']);
	$uid = intval($_SESSION['user_id']);
	
	if(isset($_POST['newcode'])) {
	
		$query = $db->query("SELECT count(*) as number
			FROM `user_relations`
			WHERE `user_id` = '$uid'
			AND `customer_id` = '$cid'
			AND `relation_privilege` > 2");
		if(!mysqli_fetch_assoc($query)['number'] > 0) {
			$error = true;
			$noaccess = true;
		}
		if(empty($_POST['newcode'])) {
			$error = true;
			$emptycodeerror = true;
		}
		if(!intval($_POST['newprivilege']) > 0) {
			$error = true;
			$priverror = true;
		}
		if(!isset($error)) {
			$cid = intval($customer['customer_id']);
			$newcode = mysqli_real_escape_string($db, $_POST['newcode']);
			$priv = intval($_POST['newprivilege']);
			
			$checkquery = $db->query("SELECT count(*) as number FROM `regcode`
				WHERE `regcode_code` = '$newcode'");
			if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
				$error = true;
				$existerror = true;
			}
			else {
				if($db->query("INSERT INTO `regcode`
					(`customer_id`,`permission`,`regcode_code`)
					VALUES ('$cid','$priv','$newcode')")) {
					$codesuccess = true;
					$success = true;
				}
				else {
					$error = true;
					$dberror = true;
				}
			}
		}
	}
}

if(isset($error)) {
	echo '<div class="alert alert-danger" role="alert">';
	echo '<h3>Noe gikk galt</h3>';
	echo '<ul>';
	if(isset($emptycodeerror))
		echo '<li>Skriv inn en kode</li>';
	
	if(isset($priverror))
		echo '<li>For lavt tilgangsnivå</li>';
	
	if(isset($dberror))
		echo '<li>Greide ikke å lagre koden. Det kan hende noen andre allerede bruker den koden.</li>';
	
	
	if(isset($existerror))
		echo '<li>Denne koden er ikke tilgjengelig.</li>';
	
	
	if(isset($noaccess))
		echo '<li>Du har ikke tilgang til å opprette koder for denne kunden.</li>';
	
	echo '</ul>';
	echo '</div>';
}

if(isset($success)) {
	if(isset($codesuccess)) {
		echo '<div class="alert alert-success" role="alert">Koden ble opprettet</div>';
	}
}

if(!empty($_GET['rm'])) {
	$rmid = intval($_GET['rm']);
	$uid = intval($_SESSION['user_id']);
	$rmfetch = $db->query("SELECT count(*) as number FROM `user_relations`,`regcode`
		WHERE `user_relations`.`user_id` = '$uid'
		AND `user_relations`.`customer_id` = `regcode`.`customer_id`
		AND `regcode`.`regcode_id` = '$rmid'");
	if($rmfetch) {
		if(mysqli_fetch_assoc($rmfetch)['number']) {
		if(!$db->query("DELETE FROM `regcode` WHERE `regcode_id` = '$rmid'"))
			error_log('SMSTavla: '.mysqli_error($db));
		}
	}
	else error_log('SMSTavla: '.mysqli_error($db));
}

// Show codes for customer:
if(isset($_GET['id']) || count($_SESSION['customers']) === 1) {
	if(isset($_GET['id'])) $customer = $_SESSION['customers'][intval($_GET['id'])];
	else $customer = $_SESSION['customers'][0];
	if($customer['relation_privilege'] >= 3) {
		$id = intval($customer['customer_id']);
		$query = $db->query("SELECT * 
			FROM `regcode` 
			WHERE `customer_id` = '$id'");
		if(mysqli_num_rows($query))
			include('templates/regcode_list.php');
		else
			include('templates/new_regcode.php');
	}
	else include('templates/noaccess.php');
}

// List customers:
else {
	if(empty($_SESSION['customers'])) include('templates/noaccess.php');
	elseif($_SESSION['highestrank'] >= 3) {
		$c = 0;
		echo '<div class="list-group">';
		foreach(array_keys($_SESSION['customers']) as $cid) {
			if($_SESSION['customers'][$cid]['relation_privilege'] >= 3) {
				$c++;
				echo '<a href="?id='.$cid.'" class="list-group-item list-group-item-action">';
				echo htmlspecialchars($_SESSION['customers'][$cid]['customer_name']);
				echo '</a>';
			}
		}
		echo '</div>';
	}
	else include('templates/noaccess.php');
}
include('templates/bottom.php');
