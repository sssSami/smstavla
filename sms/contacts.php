<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$db_con = true;
$showmenu = true;
$pagetitle = 'Kontakter';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
$cids = array();
foreach($_SESSION['customers'] as $c) {
	if($c['relation_privilege'] >= 1) {
		$cids[] = intval($c['customer_id']);
	}
}
if(!empty($cids)) {
	$imploded = implode(',',$cids);
if(!empty($_GET['id'])) {
	$id = intval($_GET['id']);
	
	$checkquery = $db->query("SELECT count(*) as number FROM `groups`
	WHERE `customer_id` IN ($imploded)
	AND `group_id` = '$id'");
	if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
		$query = $db->query("SELECT * FROM `group_members` WHERE `group_id` = '$id'");
		echo '<table class="table">';
		echo '<thead>';
		echo '<tr><th>Navn</th><th>Nummer</th></tr>';
		echo '</thead>';
		echo '<tbody>';
		$numbers = array();
		if($query) while($row = mysqli_fetch_assoc($query)) {
			$numbers[] = 'n[]='.urlencode($row['member_number']);
			echo '<tr>';
			echo '<td>'.htmlspecialchars($row['member_name']).'</td>';
			echo '<td>'.htmlspecialchars($row['member_number']).'</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		
		echo '</table>';
		echo '<a href="sendsms.php?'.implode('&amp;',$numbers).'" class="btn btn-primary">Send melding</a> ';
		echo '<a href="new_contact.php?id='.intval($_GET['id']).'" class="btn btn-primary">Ny kontakt</a>';
	}
	else include('templates/noaccess.php');
}
else {

		$query = $db->query("SELECT `groups`.*,`customers`.`customer_id`,`customers`.`customer_name`
		FROM `groups`,`customers`
		WHERE `customers`.`customer_id` = `groups`.`customer_id`
		AND `customers`.`customer_id` IN ($imploded)");
		echo '<h2>Grupper</h2>';
		echo '<table class="table">';
		echo '<thead><tr><th>Navn</th><th>Beskrivelse</th><th>Kunde</th></tr></thead>';
		echo '<tbody>';
		if($query) while($row = mysqli_fetch_assoc($query)) {
			echo '<tr>';
			echo '<td><a href="?id='.intval($row['group_id']).'">'.htmlspecialchars($row['group_name']).'</a></td>';
			echo '<td>'.htmlspecialchars($row['group_description']).'</td>';
			echo '<td>'.htmlspecialchars($row['customer_name']).'</td>';
			echo '</tr>';
		}
		
		echo '</tbody>';
		echo '</table>';
	}
}

include('templates/bottom.php');
