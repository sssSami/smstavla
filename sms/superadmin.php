<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$db_con = true;
$showmenu = true;
$pagetitle = 'Superadmin';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
if($_SESSION['user_siteadmin'] !== '1') include('templates/noaccess.php');
else {
	if(isset($_POST['addfunds-customer']) && isset($_POST['addfunds-amount'])) {
		$amount = intval($_POST['addfunds-amount']);
		$cid = intval($_POST['addfunds-customer']);
		if($db->query("UPDATE `customers`
			SET `customer_balance` = (`customer_balance`+$amount)
			WHERE `customer_id` = '$cid'")) {
				$success = true;
				$addfunds = true;
				if(!$db->query("INSERT INTO `transactions` (`customer_id`,`transaction_amount`) VALUES
				('$cid','$amount')")) {
					error_log('SMSTavla: '.mysqli_error($db));
					$error = true;
					$dberror = true;
				}
			}
			else {
				$error = true;
				$dberror = true;
				error_log('SMSTavla: '.mysqli_error($db));
			}
	}
	
	if(!empty($_POST['limit-customer']) && isset($_POST['limit-amount'])) {
		$amount = intval($_POST['limit-amount']);
		$cid = intval($_POST['limit-customer']);
		if($db->query("UPDATE `customers`
			SET `customer_chargelimit` = '$amount'
			WHERE `customer_id` = '$cid'")) {
				$success = true;
				$chlimit = true;
			}
			else {
				$error = true;
				$dberror = true;
				error_log('SMSTavla: '.mysqli_error($db));
			}
	}
	
	$query = $db->query('SELECT * FROM `customers`');
	$customers = array();
	while($row = mysqli_fetch_assoc($query)) {
		$customers[] = $row;
	}
	if(isset($error)) {
		echo '<div class="alert alert-danger" role="alert">';
		echo '<h3>Noe gikk galt</h3>';
		echo '<ul>';
		if(isset($dberror))
			echo '<li>Databasefeil: Se serverlogg</li>';
		echo '</ul>';
		echo '</div>';
	}
	if(isset($success)) {
		echo '<div class="alert alert-success" role="alert">';
		echo 'Vellykket operasjon';
		echo '</div>';
	}
?>
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-header">
				Legg til balanse
			</div>
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<label for="addfunds-amount">Beløp</label>
						<input class="form-control" type="number" name="addfunds-amount" id="addfunds-amount" value="0" required>
					</div>
					<div class="form-group">
						<label for="addfunds-customer">Kunde</label>
						<select class="form-control" name="addfunds-customer">
						<?php
						foreach($customers as $customer) {
							echo '<option value="'.intval($customer['customer_id']).'">'.htmlspecialchars($customer['customer_name']).'</option>';
						}
						?>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Legg til</button>
				</form>
			</div>
		</div>
	</div>
	<div class="col">
		<div class="card">
			<div class="card-header">
				Tillat overtrekking
			</div>
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<label for="limit-amount">Minimumsbeløp</label>
						<input class="form-control" type="number" name="limit-amount" id="addfunds-amount" value="0" max="0" min="<?= PHP_INT_MIN ?>" required>
					</div>
					<div class="form-group">
						<label for="limit-customer">Kunde</label>
						<select class="form-control" name="limit-customer">
						<?php
						foreach($customers as $customer) {
							echo '<option value="'.intval($customer['customer_id']).'">'.htmlspecialchars($customer['customer_name']).'</option>';
						}
						?>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Endre</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
}
include('templates/bottom.php');
