<?php
$pagetitle = 'Ny bruker';
session_start();
if(!empty($_POST)) $db_con = true;
$output_mode = 'html';
include('config.inc.php');
include('system.inc.php');
if(!empty($_POST['name'])
	&& !empty($_POST['email']) 
	&& !empty($_POST['password']) 
	&& !empty($_POST['password2']) 
	&& !empty($_POST['code'])) {
	if($_POST['password'] !== $_POST['password2']) {
		$matcherror = true;
		$error = true;
	}
	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$emailerror = true; // Invalid email address error
		$error = true;
	}
	$email = mysqli_real_escape_string($db, $_POST['email']);
	$query = $db->query("SELECT COUNT(*)
		as matches FROM `users`
		WHERE `user_email` = '$email'");
	$result = mysqli_fetch_assoc($query);
	if($result['matches'] > 0) {
		$emailexisterror = true; // Email already registered error
		$error = true;
	}
	
	$code = mysqli_real_escape_string($db, $_POST['code']);
	$query = $db->query("SELECT COUNT(*)
		as matches FROM `regcode`
		WHERE `regcode_code` = '$code'");
	$result = mysqli_fetch_assoc($query);
	if($result['matches'] <= 0) {
		$codeerror = true; // Invalid code for registering error
		$error = true;
	}
	
	if(strlen($_POST['password']) < 5) {
		$pwdlenerror = true; // Password length error
		$error = true;
	}
	
	if(!isset($error)) {
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$code = mysqli_real_escape_string($db, $_POST['code']);
		$pass = mysqli_real_escape_string($db, 
			password_hash($_POST['password'], PASSWORD_DEFAULT));
		$customerquery = mysqli_fetch_assoc($db->query("SELECT * 
			FROM `regcode`
			WHERE `regcode`.`regcode_code` = '$code'
			LIMIT 1"));
		$customerid = intval($customerquery['customer_id']);
		$privilege = intval($customerquery['permission']);
		if($db->query("INSERT INTO `users`
			(`user_name`,`user_email`,`user_password`)
			VALUES('$name','$email','$pass')")) {
				$newuid = intval($db->insert_id);
				if($db->query("INSERT INTO `user_relations`
				(`user_id`,`customer_id`,`relation_privilege`)
				VALUES('$newuid','$customerid','$privilege')"))
					$success = true;
			}
				
			else {
				$error = true;
				$dberror = true;
				error_log('SMSTavla: New user creation: '
					.mysqli_error($db));
			}
	}
	
}
if(isset($success)) header('Location: login.php?reg=1&email='.urlencode($_POST['email']));
include('templates/top.php');

if(isset($error)) {
	echo '<div class="alert alert-danger" role="alert">';
	echo '<h4 class="alert-heading">Prøv igjen</h4>';
	echo '<ul>';
	if(isset($dberror))
		echo '<li>Intern databasefeil. 
			Brukeren ble ikke opprettet.</li>'.(isset($debug)
				? ('<li>'.mysqli_error($db).'</li>'):'');
	if(isset($matcherror))
		echo '<li>Passordene var ikke like!</li>';
	if(isset($pwdlenerror))
		echo '<li>Passordet er for kort!</li>';
	if(isset($emailexisterror))
		echo '<li>E-postadressen er allerede registrert!</li>';
	if(isset($emailerror))
		echo '<li>E-postadressen er ikke gyldig!</li>';
	if(isset($codeerror))
		echo '<li>Registreringskoden er ikke gyldig!</li>';
	echo '</ul>';
	echo '</div>';
}
if(isset($success))
	echo '<div 
		class="alert alert-success" 
		role="alert">
			Brukeren er opprettet!
		</div>';
?>
<h1>Opprett ny bruker</h1>
<form method="post">
  <div class="form-group">
    <label for="name">Navn</label>
    <input
    	type="text"
	name="name"
	class="form-control<?= isset($nameerror) ? ' is-invalid' : '' ?>"
	id="name"
	placeholder="Ditt navn"
	required>
  </div>
  <div class="form-group">
    <label for="email">E-postadresse</label>
    <input 
	    type="email" 
	    name="email" 
	    class="form-control<?= isset($emailerror) 
	    	|| isset($emailexisterror) ? ' is-invalid' : '' ?>" 
	    id="email" 
	    aria-describedby="emailHelp" 
	    placeholder="Din e-postadresse" 
	    required>
  </div>
  <div class="form-group mb-3">
    <label for="password">Passord</label>
    <input 
    	type="password" 
    	name="password" 
    	class="form-control<?= isset($matcherror)
    		|| isset($pwdlenerror) ? ' is-invalid' : '' ?>" 
    	id="password" 
    	placeholder="Ditt passord" 
    	required>
  </div>
  <div class="form-group">
    <label for="password2">Passord</label>
    <input 
    	type="password" 
    	name="password2" 
    	class="form-control<?= isset($matcherror) ? ' is-invalid' : '' ?>" 
    	id="password2" 
    	placeholder="Gjenta passord" 
    	required>
  </div>
  <div class="form-group">
    <label for="code">Kode</label>
    <input 
    	type="text" 
    	name="code" 
    	class="form-control<?= isset($codeerror) ? ' is-invalid' : '' ?>" 
    	id="code" 
    	placeholder="Registreringskode" 
		value="<?= @htmlspecialchars($_GET['regcode']) ?>"
    	required>
  </div>
  <button type="submit" class="btn btn-primary">Opprett</button>
  <a href="login.php" class="btn btn-light">Tilbake</a>
</form>
<?php
include('templates/bottom.php');
