<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
if(!empty($_POST)) $db_con = true;
$pagetitle = 'Medlemskap';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
if(!empty($_POST['code']) && !empty($_POST['password'])) {
	$uid = intval($_SESSION['user_id']);
	$code = mysqli_real_escape_string($db, trim($_POST['code']));
	$codequery = $db->query("SELECT * FROM `regcode`
	WHERE `regcode_code` = '$code'
	AND `customer_id` NOT IN
		(SELECT `customer_id`
		FROM `user_relations`
		WHERE `user_id` = '$uid')");
	if(!$codequery) {
		$error = true;
		$dberror = true;
		error_log('SMSTavla: '.mysqli_error($db));
	}
	else {
		$coderesult = mysqli_fetch_assoc($codequery);
		$cid = intval($coderesult['customer_id']);
		$priv = intval($coderesult['permission']);
	}
	if(!mysqli_num_rows($codequery)) {
		$error = true;
		$invcode = true;
	}
	if(!password_verify($_POST['password'], $_SESSION['user_password'])) {
		$error = true;
		$passwderror = true;
	}
	if(!isset($error) && isset($coderesult)) {
		if($db->query("INSERT INTO `user_relations` (`user_id`,`customer_id`,`relation_privilege`)
			VALUES ('$uid','$cid','$priv')")) {
			$success = true;
			if($customerquery = $db->query("SELECT * FROM `customers`,`user_relations`
			WHERE `customers`.`customer_id` = '$cid'
			AND `customers`.`customer_id` = `user_relations`.`customer_id`
			AND `user_relations`.`user_id` = '$uid'")) {
				$_SESSION['customers'][] = mysqli_fetch_assoc($customerquery);
			}
			else error_log('SMSTavla: '.mysqli_error($db));
		}
	}
}
echo '<h2>Medlemskap</h2>';
echo '<div class="card-group">';
foreach($_SESSION['customers'] as $customer) {
	include('templates/customer_card.php');
}
echo '</div>';
if(isset($error)) {
		echo '<div class="alert alert-danger" role="alert">';
		echo '<h3>Prøv igjen</h3>';
		echo '<ul>';
		if(isset($dberror))
			echo '<li>Databasefeil. Prøv igjen senere.</li>';
		if(isset($passwderror))
			echo '<li>Passordet stemmer ikke.</li>';
		if(isset($invcode))
			echo '<li>Ugyldig registreringskode. Koden finnes ikke, eller du er allerede medlem hos denne kunden.</li>';
		echo '</ul>';
		echo '</div>';
}
if(isset($success))
	echo '<div class="alert alert-success" role="alert">Du er innmeldt!</div>';
include('templates/new_membership.php');
include('templates/bottom.php');
