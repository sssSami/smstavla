<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
if(isset($_GET['cid'])) $db_con = true;
$pagetitle = 'Brukeradministrasjon';
include('config.inc.php');
include('system.inc.php');
if(!isset($_GET['cid']) && count($_SESSION['customers']) === 1) {
	header('Location: ?cid=0');
	die();
}
include('templates/top.php');

if(isset($_GET['cid'])) {
	if(isset($_SESSION['customers'][$_GET['cid']]) && $_SESSION['customers'][$_GET['cid']]['relation_privilege'] >= 3) {
		echo '<div class="clearfix">';
		echo '<h2 class="float-left">Brukere</h2>';
		echo count($_SESSION['customers']) > 1 ? '<a href="adm_users.php" class="btn btn-link float-right">Tilbake</a>' : '';
		echo '</div>';
		$cid = intval($_SESSION['customers'][$_GET['cid']]['customer_id']);
		$query = $db->query("SELECT * FROM `users`,`user_relations`
		WHERE `users`.`user_id` = `user_relations`.`user_id`
		AND `user_relations`.`customer_id` = '$cid'");
		echo '<ul>';
		while($row = mysqli_fetch_assoc($query)) {
			echo '<li>';
			echo htmlspecialchars($row['user_name']);
			echo '</li>';
		}
		echo '</ul>';
	}
	else include('templates/noaccess.php');
}
else {
	echo '<div class="list-group">';
	foreach(array_keys($_SESSION['customers']) as $cid) {
		if($_SESSION['customers'][$cid]['relation_privilege'] >= 3) {
			echo '<a href="?cid='.intval($cid).'" class="list-group-item list-group-item-action">';
			echo htmlspecialchars($_SESSION['customers'][$cid]['customer_name']);
			echo '</a>';
		}
	}
	echo '</div>';
}

include('templates/bottom.php');
