<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$db_con = true;
$showmenu = true;
$pagetitle = 'Kontakter';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');

$cids = array();
foreach($_SESSION['customers'] as $c) {
	if($c['relation_privilege'] >= 1) {
		$cids[] = intval($c['customer_id']);
	}
}
if(!empty($cids)) 
	$imploded = implode(',',$cids);

$id = intval($_GET['id']);
$checkquery = $db->query("SELECT count(*) as number FROM `groups`
WHERE `customer_id` IN ($imploded)
AND `group_id` = '$id'");
if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
	if(!empty($_POST['name']) && !empty($_POST['number'])) {
		$name = mysqli_real_escape_string($db, trim($_POST['name']));
		$number = mysqli_real_escape_string($db, trim($_POST['number']));
		if($db->query("INSERT INTO `group_members` (`group_id`,`member_name`,`member_number`)
			VALUES ('$id','$name','$number')")) {
				echo '<div class="alert alert-success" role="alert">';
				echo 'Kontakten er lagt til i gruppen';
				echo '</div>';
			}
		else {
			echo '<div class="alert alert-danger" role="alert">';
			echo 'Databasefeil. Prøv igjen senere.';
			echo '</div>';
			error_log('SMSTavla: '.mysqli_error($db));
		}
	}
?>
<form action="" method="post">
	<div class="form-group">
		<label for="name">Navn</label>
		<input type="text" name="name" id="name" class="form-control" required>
	</div>
	<div class="form-group">
		<label for="number">Nummer</label>
		<input type="text" name="number" id="number" class="form-control" required>
	</div>
	<button type="submit" class="btn btn-primary">Lagre</button>
	<a href="contacts.php?id=<?= intval($_GET['id']) ?>" class="btn btn-light">Tilbake</a>
</form>
<?php
}
else include('templates/noaccess.php');
include('templates/bottom.php');
