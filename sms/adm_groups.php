<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$db_con = true;
$showmenu = true;
$pagetitle = 'Kontakter';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
$cids = array();
foreach($_SESSION['customers'] as $c) {
	if($c['relation_privilege'] >= 1) {
		$cids[] = intval($c['customer_id']);
	}
}
$imploded = implode(',',$cids);
if($_SESSION['highestrank'] >= 3) {
	if(!empty($_GET['rm'])) {
		$rmid = intval($_GET['rm']);
		$checkquery = $db->query("SELECT count(*) as number FROM `groups`
		WHERE `customer_id` IN ($imploded)
		AND `group_id` = '$rmid'");
		if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
			if($db->query("DELETE FROM `groups`
			WHERE `group_id` = '$rmid'
			AND `customer_id` IN ($imploded)")) {
				$success = true;
				$rmgroup = true;
			}
			else {
				$dberror = true;
				error_log('SMSTavla: '.mysqli_error($db));
			}
		}
	}
	if(!empty($_POST['cid']) && !empty($_POST['name']) && isset($_POST['description'])) {
		if(isset($_SESSION['customers'][$_POST['cid']])) {
			$cid = intval($_SESSION['customers'][$_POST['cid']]['customer_id']);
			$name = mysqli_real_escape_string($db, trim($_POST['name']));
			$description = mysqli_real_escape_string($db, trim($_POST['description']));
			$checkquery = $db->query("SELECT count(*) as number FROM `groups`
			WHERE `customer_id` = '$cid'
			AND `group_name` = '$name'");
			if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
				$error = true;
				$exists = true;
			}
			else {
				if($db->query("INSERT INTO `groups` (`customer_id`,`group_name`,`group_description`)
					VALUES ('$cid','$name','$description')")) {
					$success = true;
				}
				else {
					$error = true;
					$dberror = true;
					error_log('SMSTavla: '.mysqli_error($db));
				}
			}
		}
	}
	if(isset($dberror)) {
		echo '<div class="alert alert-danger" role="alert">';
		echo 'Databasefeil. Prøv igjen senere.';
		echo '</div>';
	}
	if(isset($exists)) {
		echo '<div class="alert alert-danger" role="alert">';
		echo 'Denne gruppen finnes allerede.';
		echo '</div>';
	}
	if(isset($success)) {
		echo '<div class="alert alert-success" role="alert">';
		echo isset($rmgroup) ? 'Gruppen ble slettet' : 'Gruppen ble opprettet';
		echo '</div>';
	}

	if(!empty($cids)) {

		$query = $db->query("SELECT `groups`.*,`customers`.`customer_id`,`customers`.`customer_name`
		FROM `groups`,`customers`
		WHERE `customers`.`customer_id` = `groups`.`customer_id`
		AND `customers`.`customer_id` IN ($imploded)");
		echo '<h2>Grupper</h2>';
		echo '<table class="table">';
		echo '<thead><tr><th>Navn</th><th>Beskrivelse</th><th>Kunde</th><th>Slett</th></tr></thead>';
		echo '<tbody>';
		if($query) while($row = mysqli_fetch_assoc($query)) {
			echo '<tr>';
			echo '<td><a href="contacts.php?id='.intval($row['group_id']).'">'.htmlspecialchars($row['group_name']).'</a></td>';
			echo '<td>'.htmlspecialchars($row['group_description']).'</td>';
			echo '<td>'.htmlspecialchars($row['customer_name']).'</td>';
			echo '<td><a href="?rm='.intval($row['group_id']).'">Slett</a></td>';
			echo '</tr>';
		}
		
		echo '</tbody>';
		echo '</table>';
	}
	include('templates/new_group.php');
}
else include('templates/noaccess.php');
include('templates/bottom.php');
