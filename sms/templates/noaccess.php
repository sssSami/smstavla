
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">Ingen adgang!</h4>
  <p>
  	Oisann! Denne siden har du ikke lov å se.
  	Hvis du mener dette er ved en feil kan du spørre arbeidsgiver eller
  	ta kontakt med nettstedseier.
</p>
<a href="./" class="btn btn-primary btn-lg btn-block">Til forsiden</a>
</div>

