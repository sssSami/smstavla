
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">SMSTavla</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="sendsms.php">Send SMS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contacts.php">Kontakter</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Min bruker
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="chpassword.php">Endre passord</a>
          <a class="dropdown-item" href="memberships.php">Mine medlemskap</a>
        </div>
      </li>
	  <?php
	  if($_SESSION['highestrank'] >= 2) {
	  ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Administrasjon
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<?php
		
		if($_SESSION['user_siteadmin'] === '1') echo '<a class="dropdown-item" href="superadmin.php">Superadmin</a>';
		
		if($_SESSION['highestrank'] >= 3) {
			echo '
          <a class="dropdown-item" href="adm_users.php">Administrer brukere</a>
          <a class="dropdown-item" href="adm_groups.php">Administrer grupper</a>
          <a class="dropdown-item" href="adm_regcodes.php">Registreringskoder</a>
		  ';
		}
		if($_SESSION['highestrank'] >= 2) echo '<a class="dropdown-item" href="adm_balance.php">Kreditt og balanse</a>';
		
		  ?>
		  
          <a class="dropdown-item" href="adm_queue.php">Meldingskø</a>
          <a class="dropdown-item" href="history.php">Historikk</a>
        </div>
      </li>
	  <?php
	  }
	  ?>
      <li class="nav-item">
        <a class="nav-link" href="logout.php">Logg ut</a>
      </li>
    </ul>
  </div>
</nav>
