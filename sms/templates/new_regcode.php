<form action="" method="post">
<h3>Ny kode</h3>
	<div class="form-group">
		<label for="new-regcode">Kode</label>
		<input type="text" id="new-regcode" name="newcode" class="form-control" required>
	</div>
	<div class="form-group">
		<label for="new-privilege">Tilgangsnivå</label>
		<select id="new-privilege" name="newprivilege" class="form-control">
			<?php
			foreach(array_keys($ranks) as $rank) {
				echo "<option value=\"$rank\">{$ranks[$rank]}</option>";
			}
			?>
		</select>
	</div>
	<button class="btn btn-primary" type="submit">Opprett</button>
</form>