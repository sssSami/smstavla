<div class="">
<?= '<h3 class="float-left">'.htmlspecialchars($customer['customer_name']).'</h3>' ?>
<?= isset($_GET['id']) ? '<a href="adm_regcodes.php" class="btn btn-link float-right">Tilbake</a>' : '' ?>
</div>
<table class="table">
	<thead>
		<tr>
			<th scope="col">Kode</th>
			<th scope="col">Tilgangsnivå</th>
			<th scope="col">Slett</th>
		</tr>
	</thead>
	<tbody>
	<?php
	while($row = mysqli_fetch_assoc($query)) {
		echo '<tr>';
		echo '<td>'.$row['regcode_code'].'</td>';
		echo '<td>'.$ranks[$row['permission']].'</td>';
		echo '<td><a href="?rm='.intval($row['regcode_id']).(isset($_GET['id']) ? '&amp;id='.intval($_GET['id']) : '').'">Slett</a></td>';
		echo '</tr>';
	}
	?>
	</tbody>
</table>
<?php include('new_regcode.php'); ?>