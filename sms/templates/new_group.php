<form action="" method="post" class="form-inline">
	<div class="form-group mb-2">
		<label for="customerpick" class="sr-only">Kunde</label>
		<select name="cid" id="customerpick" class="form-control">
			<?php
				foreach(array_keys($_SESSION['customers']) as $cid) {
					echo '<option value="'.$cid.'">';
					echo htmlspecialchars($_SESSION['customers'][$cid]['customer_name']);
					echo '</option>';
				}
			?>
		</select>
	</div>
	<div class="form-group mx-sm-3 mb-2">
		<label for="newname" class="sr-only">Navn på ny gruppe</label>
		<input type="text" name="name" id="newname" class="form-control" placeholder="Navn på gruppe" required>
	</div>
	<div class="form-group mx-sm-3 mb-2">
		<label for="description" class="sr-only">Beskrivelse</label>
		<input type="text" name="description" id="description" class="form-control" placeholder="Beskrivelse">
	</div>
	<button type="submit" class="btn btn-primary">Opprett</button>
</form>