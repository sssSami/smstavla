
	<div class="card">
	  <div class="card-header">
		<?= htmlspecialchars($customer['customer_name']) ?>
	  </div>
	  <div class="card-body">
		<h5 class="card-title">
			<?= htmlspecialchars($customer['customer_email']) ?>
		</h5>
		<p class="card-text">
			<?= $ranks[$customer['relation_privilege']] ?>
		</p>
	  </div>
	</div>
