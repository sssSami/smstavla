<h2>Nytt medlemskap</h2>
<form action="" method="post" class="form-inline">
	<div class="form-group mb-2">
		<label for="code" class="sr-only">Kode</label>
		<input type="text" class="form-control" placeholder="Registreringskode" name="code" id="code" required>
	</div>
	<div class="form-group mx-sm-3 mb-2">
		<label for="password" class="sr-only">Ditt passord</label>
		<input type="password" class="form-control" placeholder="Ditt passord" name="password" id="password" required>
	</div>
	<button type="submit" class="btn btn-primary">OK</button>
</form>