<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
$db_con = true;
//$pagetitle = 'Testside';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');

if(!isset($_GET['id'])) {
	$uid = intval($_SESSION['user_id']);
	$query = $db->query("SELECT *
		FROM `user_relations`,`customers`
		WHERE `user_relations`.`customer_id` = `customers`.`customer_id`
		AND `user_relations`.`user_id` = '$uid'");
		$_SESSION['customers'] = array();
	while($row = mysqli_fetch_assoc($query)) { // Dump customer data into session
		$_SESSION['customers'][] = $row;
	}
}

if($_SESSION['highestrank'] >= 2) {
	if(isset($_GET['id'])) {
		$uid = intval($_SESSION['user_id']);
		$cid = intval($_GET['id']);
		$checkquery = $db->query("SELECT count(*) as number
			FROM `user_relations`
			WHERE `user_id` = '$uid'
			AND `customer_id` = '$cid'");
		if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
			$query = $db->query("SELECT * FROM `transactions` WHERE `customer_id` = '$cid'");
			
			echo '<div class="clearfix">';
			echo '<h2 class="float-left">Transaksjoner</h2>';
			echo '<a href="adm_balance.php" class="btn btn-link float-right">Tilbake</a>';
			echo '</div>';
			echo '<table class="table">';
			echo '<thead><tr><th>Beløp</th><th>Tidspunkt</th></tr></thead>';
			
			echo '<tbody>';
			while($row = mysqli_fetch_assoc($query)) {
				echo '<tr>';
				echo '<td class="'.($row['transaction_amount']<0 ? 'text-danger' : 'text-success').'">';
				echo number_format($row['transaction_amount'], 2, ',',' ');
				echo '</td>';
				
				echo '<td>';
				echo date('d.m.Y H:i', strtotime($row['transaction_date']));
				echo '</td>';
				echo '</tr>';
			}
			
			echo '</tbody>';
			echo '</table>';
		}
		else include('templates/noaccess.php');
	}
	else {
		echo '<h2>Kreditt og balanse</h2>';
		echo '<table class="table">';
		echo '<thead><tr><th>Navn</th><th>Balanse</th><th>Pris</th><th title="Negativt beløp betyr at det kan overtrekkes">Minimum balanse</th></tr></thead>';
		echo '<tbody>';
		foreach($_SESSION['customers'] as $customer) {
			echo '<tr>';
			echo '<td><a href="?id='.intval($customer['customer_id']).'">'.htmlspecialchars($customer['customer_name']).'</a></td>';
			echo '<td>'.number_format($customer['customer_balance'], 2, ',',' ').'</td>';
			echo '<td>'.number_format($customer['customer_price'], 2, ',',' ').'</td>';
			echo '<td>'.number_format($customer['customer_chargelimit'], 2, ',',' ').'</td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '</table>';
	}
} else include('templates/noaccess.php');

include('templates/bottom.php');
