<?php
$output_mode = 'html';
include('config.inc.php');
include('system.inc.php');
if(!$debug || !$debug_newusers) die('Not allowed');

?><!DOCTYPE html>
<html>
<head>
<title>Create user</title>
<meta charset="UTF-8">
</head>
<body>
<form action="" method="post">
<input type="password" name="password"><button>Submit</button>
</form>
<?php
if(isset($_POST['password'])) {
	echo '<p>'.password_hash($_POST['password'], PASSWORD_DEFAULT).'</p>';
}
?>
</body>
</html>