<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
$pagetitle = 'Endre passord';

if(!empty($_POST)) $db_con = true;

include('config.inc.php');
include('system.inc.php');

if(!empty($_POST['oldpassword'])
	&& !empty($_POST['newpassword']) 
	&& !empty($_POST['repassword'])) {
	if($_POST['repassword'] !== $_POST['newpassword']) {
		$matcherror = true;
		$error = true;
	}
	
	if(!password_verify($_POST['oldpassword'], $_SESSION['user_password'])) {
			$olderror = true;
			$error = true;
		}
	
	if(!isset($error)) {
		if($_SESSION['user_password'] = 
			password_hash($_POST['newpassword'], PASSWORD_DEFAULT)) {
			$pwd = mysqli_real_escape_string(
				$db, 
				$_SESSION['user_password']
			);
			$uid = intval($_SESSION['user_id']);
			if($db->query("UPDATE `users` 
				SET `user_password` = '$pwd' 
				WHERE `user_id` = '$uid'"))
				$success = true;
			else {
				$dberror = true;
				$error = true;
				error_log('SMSTavla: Changing password failed: '.
					mysqli_error($db));
			}
		}
	}
}
include('templates/top.php');
if(isset($error)) {
	echo '<div class="alert alert-danger" role="alert">';
	echo '<h4 class="alert-heading">Prøv igjen</h4>';
	echo '<ul>';
	if(isset($dberror))
		echo '<li>En feil oppstod når vi forsøkte å oppdatere passordet
		i databasen</li>';
	if(isset($matcherror))
		echo '<li>Passordene var ikke like!</li>';
	if(isset($olderror))
		echo '<li>Det gamle passordet stemte ikke!</li>';
	echo '</ul>';
	echo '</div>';
}
if(isset($success)) 
	echo '
	<div class="alert alert-success" role="alert">
  Passordet har blitt endret!
</div>
	';

?>
<h1>Endre passord</h1>
<form method="post">
  <div class="form-group">
    <label for="oldpassword">Gammelt passord</label>
    <input 
    	type="password" 
    	name="oldpassword" 
    	class="form-control<?= isset($olderror) ? ' is-invalid' : '' ?>" 
    	id="oldpassword" 
    	placeholder="Gammelt passord" 
    	required>
  </div>
  <div class="form-group">
    <label for="newpassword">Nytt passord</label>
    <input 
    	type="password" 
    	name="newpassword" 
    	class="form-control<?= isset($matcherror) ? ' is-invalid' : '' ?>" 
    	id="newpassword" 
    	placeholder="Nytt passord" 
    	required>
  </div>
  <div class="form-group">
    <label for="repassword">Gjenta nytt passord</label>
    <input 
    	type="password" 
    	name="repassword" 
    	class="form-control<?= isset($matcherror) ? ' is-invalid' : '' ?>" 
    	id="repassword" 
    	placeholder="Nytt passord" 
    	required>
  </div>
  <button type="submit" class="btn btn-primary">Endre</button>
</form>
<?php
include('templates/bottom.php');
