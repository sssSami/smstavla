<?php
$pagetitle = 'Innlogging';
if(!empty($_POST)) $db_con = true; // Connect to database if POST data
$output_mode = 'html';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
if(!empty($_POST)) {
	session_start();
	$email = mysqli_real_escape_string($db, $_POST['email']);
	$pass = $_POST['password'];
	
	if($query = $db->query("SELECT * FROM `users`
		WHERE `user_email` = '$email'
		AND `user_enabled` = '1'
		LIMIT 1")) {
			
		try {
			$data = mysqli_fetch_assoc($query);
			if(password_verify($pass, $data['user_password'])) {
				// success
				$_SESSION = $data; // Dump user data into session
				$id = intval($data['user_id']);
				$query = $db->query("SELECT *
					FROM `user_relations`,`customers`
					WHERE `user_relations`.`customer_id` = `customers`.`customer_id`
					AND `user_relations`.`user_id` = '$id'");
					$_SESSION['highestrank'] = 0;
				while($row = mysqli_fetch_assoc($query)) { // Dump customer data into session
					$_SESSION['customers'][] = $row;
					if($row['relation_privilege'] > $_SESSION['highestrank'])
						$_SESSION['highestrank'] = $row['relation_privilege'];
				}
				$_SESSION['loggedin'] = true;
				header('Location: ./');
			}
			else {
				// wrong password
				$error = true;
			}
		}
		catch(Exception $e)
		{
			$error = true;
			error_log('SMSTavla: ' . $e);
		}
	}
	else $error = true;
}
if(isset($error)) {
?>
<div class="alert alert-danger" role="alert">
	E-postadresse eller passord er feil.
</div>
<?php
}
if(isset($_GET['reg']))
	echo '<div 
		class="alert alert-success" 
		role="alert">
			Brukeren er opprettet!
		</div>';
?>
<h1>Velkommen til SMSTavla!</h1>
<form method="post">
  <div class="form-group">
    <label for="email">E-postadresse</label>
    <input type="email" name="email" class="form-control<?= isset($error) ? ' is-invalid' : '' ?>" id="email" aria-describedby="emailHelp" placeholder="Din e-postadresse" value="<?= @htmlspecialchars($_GET['email']) ?>" required>
  </div>
  <div class="form-group">
    <label for="password">Passord</label>
    <input type="password" name="password" class="form-control<?= isset($error) ? ' is-invalid' : '' ?>" id="password" placeholder="Ditt passord" required>
  </div>
  <button type="submit" class="btn btn-primary">Logg inn</button> <a href="new_user.php" class="btn btn-light">Ny bruker</a> <a href="new_customer.php" class="btn btn-light">Ny kunde</a>
</form>
<?php
include('templates/bottom.php');
