<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$db_con = true;
$showmenu = true;
//$pagetitle = 'Testside';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');

$cids = array();
foreach($_SESSION['customers'] as $c) {
	if($c['relation_privilege'] >= 3) {
		$cids[] = intval($c['customer_id']);
	}
}
if(!empty($cids)) {
	$imploded = implode(',',$cids);
	$query = $db->query("SELECT `queue`.*,`customers`.`customer_id`,`customers`.`customer_name`
	FROM `queue`,`customers`
	WHERE `customers`.`customer_id` = `queue`.`customer_id`
	AND `customers`.`customer_id` IN ($imploded)
	ORDER BY `queue_id` DESC");
	echo '<h2>Historikk</h2>';
	echo '<table class="table">';
	echo '<thead><tr><th>Melding</th><th>Til</th><th>Fra</th><th>Tid</th><th>Sendt</th></tr></thead>';
	echo '<tbody>';
	while($row = mysqli_fetch_assoc($query)) {
		echo '<tr>';
		echo '<td>'.htmlspecialchars($row['queue_message']).'</td>';
		echo '<td>'.htmlspecialchars($row['queue_to']).'</td>';
		echo '<td>'.htmlspecialchars($row['customer_name']).'</td>';
		echo '<td>'.date('d.m.Y H:i', strtotime($row['queued_time'])).'</td>';
		echo '<td>'.($row['queue_sent']=='1' ? 'Ja' : 'Nei').'</td>';
		echo '</tr>';
	}
	
	echo '</tbody>';
	echo '</table>';
}


include('templates/bottom.php');
