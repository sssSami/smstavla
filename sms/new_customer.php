<?php
$pagetitle = 'Ny bruker';
session_start();
if(!empty($_POST)) $db_con = true;
$output_mode = 'html';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');

if(!empty($_POST)) {
	if(empty($_POST['name'])) {
		$noname = true;
		$error = true;
	}
	
	if(empty($_POST['email'])) {
		$nomail = true;
		$error = true;
	}
	
	if(empty($_POST['phone'])) {
		$nophone = true;
		$error = true;
	}
	
	
	if(empty($_POST['address'])) {
		$noaddr = true;
		$error = true;
	}
	
	if(empty($_POST['zip'])) {
		$nozip = true;
		$error = true;
	}
	
	if(empty($_POST['city'])) {
		$nocity = true;
		$error = true;
	}
	
	if(empty($_POST['code'])) {
		$nocode = true;
		$error = true;
	}
	
	$name = mysqli_real_escape_string($db, $_POST['name']);
	$query = $db->query("SELECT count(*) as number FROM `customers` WHERE `customer_name` = '$name'");
	if(mysqli_fetch_assoc($query)['number'] > 0) {
		$existserror = true;
		$error = true;
	}
	
	if(!isset($nocode)) {
		$newcode = mysqli_real_escape_string($db, $_POST['code']);
		$checkquery = $db->query("SELECT count(*) as number FROM `regcode`
		WHERE `regcode_code` = '$newcode'");
		if(mysqli_fetch_assoc($checkquery)['number'] > 0) {
			$codeexistserror = true;
			$error = true;
		}
	}
	
	if(!isset($error)) {
		$newcode = mysqli_real_escape_string($db, $_POST['code']);
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$phone = mysqli_real_escape_string($db, $_POST['phone']);
		$address = mysqli_real_escape_string($db, $_POST['address']);
		$address2 = !empty($_POST['address2']) ? mysqli_real_escape_string($db, $_POST['address2']) : null;
		$zip = mysqli_real_escape_string($db, $_POST['zip']);
		$city = mysqli_real_escape_string($db, $_POST['city']);
		if($db->query("INSERT INTO `customers`
			(`customer_name`,`customer_email`,`customer_phone`,`customer_city`,`customer_zip`,`customer_address`,`customer_address2`)
			VALUES('$name','$email','$phone','$city','$zip','$address','$address2')")) {
			$cid = intval($db->insert_id);
			if($db->query("INSERT INTO `regcode` (`customer_id`,`permission`,`regcode_code`)
				VALUES('$cid','3','$newcode')")) {
				$success = true;
			}
			else {
				$dberror = true;
				$error = true;
				$dbcodeerror = true;
				error_log('SMSTavla: '.mysqli_error($db));
			}
		}
		else {
			$dberror = true;
			$error = true;
			error_log('SMSTavla: '.mysqli_error($db));
		}
	}
	
}

if(isset($error)) {
	echo '<div class="alert alert-danger" role="alert">';
	echo '<h3>Prøv igjen</h3>';
	echo '<ul>';
	if(isset($noname))
		echo '<li>Du har ikke fyllt ut navn</li>';
	if(isset($existserror))
		echo '<li>Navnet eksisterer allerede</li>';
	if(isset($nomail))
		echo '<li>Du har ikke fyllt ut e-postadressen</li>';
	if(isset($nophone))
		echo '<li>Du har ikke fyllt ut telefonnummer</li>';
	if(isset($noaddr))
		echo '<li>Du har ikke fyllt ut adresse</li>';
	if(isset($nozip))
		echo '<li>Du har ikke fyllt ut postnummeret</li>';
	if(isset($nocode))
		echo '<li>Du har ikke fyllt ut registreringskode</li>';
	if(isset($codeexistserror))
		echo '<li>Denne registreringskoden er ikke tilgjengelig</li>';
	echo '</ul>';
	echo '</div>';
}

if(isset($success))
	echo '<div 
		class="alert alert-success" 
		role="alert">
			<p>Kundeprofilen er opprettet!</p>
			<a href="new_user.php?regcode='.htmlspecialchars($_POST['code']).'" class="btn btn-primary btn-lg btn-block">Opprett bruker</a>
		</div>';

?>
<h1>Ny kunde</h1>
<form method="post">
  <div class="form-group">
    <label for="name">Navn</label>
    <input
    	type="text"
	name="name"
	class="form-control<?= isset($noname) || isset($existserror) ? ' is-invalid' : '' ?>"
	id="name"
	placeholder="Bedriftsnavn eller lignende"
	required>
  </div>
  <div class="form-group">
    <label for="email">E-postadresse</label>
    <input 
	    type="email" 
	    name="email" 
	    class="form-control<?= isset($nomail) ? ' is-invalid' : '' ?>" 
	    id="email" 
	    aria-describedby="emailHelp" 
	    placeholder="Din e-postadresse" 
	    required>
  </div>
  <div class="form-group">
    <label for="phone">Telefonnummer</label>
    <input
    	type="text"
	name="phone"
	class="form-control<?= isset($nophone) ? ' is-invalid' : '' ?>"
	id="phone"
	placeholder="Telefonnummer"
	required>
  </div>
  <div class="form-group">
    <label for="address">Adresse</label>
    <input
    	type="text"
	name="address"
	class="form-control<?= isset($noaddr) ? ' is-invalid' : '' ?>"
	id="address"
	placeholder="Adresse"
	required>
  </div>
  <div class="form-group">
    <label for="address2">Adresse 2</label>
    <input
    	type="text"
	name="address2"
	class="form-control<?= isset($address2error) ? ' is-invalid' : '' ?>"
	id="address2"
	placeholder="Adresse 2">
  </div>
  <div class="form-group">
    <label for="zip">Postnummer</label>
    <input
    	type="text"
	name="zip"
	class="form-control<?= isset($nozip) ? ' is-invalid' : '' ?>"
	id="zip"
	placeholder="Postnummer"
	required>
  </div>
  <div class="form-group">
    <label for="city">Poststed</label>
    <input
    	type="text"
	name="city"
	class="form-control<?= isset($nocity) ? ' is-invalid' : '' ?>"
	id="city"
	placeholder="Poststed"
	required>
  </div>
  <div class="form-group">
    <label for="code">Kode for oppretting av ny administratorbruker</label>
    <input 
    	type="text" 
    	name="code" 
    	class="form-control<?= isset($nocode) || isset($codeexistserror) ? ' is-invalid' : '' ?>" 
    	id="code" 
    	placeholder="Registreringskode"
		value="<?= rand(1000,99999999) ?>"
    	required>
  </div>
  <button type="submit" class="btn btn-primary">Opprett</button>
  <a href="login.php" class="btn btn-light">Tilbake</a>
</form>
<?php
include('templates/bottom.php');
