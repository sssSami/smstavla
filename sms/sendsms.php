<?php
session_start();
$enforce_auth = true;
$output_mode = 'html';
$showmenu = true;
if(!empty($_POST)) $db_con = true;
$pagetitle = 'Send SMS';
include('config.inc.php');
include('system.inc.php');
include('templates/top.php');
if($_SESSION['highestrank'] < 1)
	{
	include('templates/noaccess.php');
	}
else
	{

if(!empty($_POST)) {
	if(empty($_POST['customer'])) {
		$nocusterr = true;
		$error = true;
	}
	else {
			$yes = false;
		foreach($_SESSION['customers'] as $customer) {
			if(intval($customer['customer_id']) === intval($_POST['customer'])) {
				$yes = true;
				break;
			}
		}
		if(!$yes) {
			$invcusterr = true;
			$error = true;
		}
	}
	if(empty($_POST['recipients'])) {
		$norecerr = true;
		$error = true;
	}
	elseif(!isset($error)) {
		$recipients = explode("\n", str_replace("\r", '', trim($_POST['recipients'])));
		$query = 'INSERT INTO `queue`(`user_id`,`customer_id`,`queue_to`,`queue_message`) VALUES';
		$uid = intval($_SESSION['user_id']);
		$cid = intval($_POST['customer']);
		$msg = mysqli_real_escape_string($db, $_POST['message']);
		$c = 0;
		foreach($recipients as $rec) {
			$c++;
			$clean = mysqli_real_escape_string($db, $rec);
			$query .= "\n('$uid','$cid','$clean','$msg')".($c<count($recipients) ? ',' : '');
		}
		
		$checkquery = $db->query("SELECT
		`customer_balance`,`customer_chargelimit`,`customer_price`
		FROM `customers`
		WHERE `customer_id` = '$cid' LIMIT 1");
		$checkdata = mysqli_fetch_assoc($checkquery);
		$charge = $checkdata['customer_price'] * count($recipients);
		if($checkdata['customer_balance'] - $charge < $checkdata['customer_chargelimit']) {
			$error = true;
			$chargeerror = true;
		}
		else {
			
			if($db->query($query)) {
				$success = true;
				if(!$db->query("UPDATE `customers`
					SET `customer_balance` = (`customer_balance` - $charge)
					WHERE `customer_id` = '$cid'")) error_log('SMSTavla: '.mysqli_error($db));
				if(!$db->query("INSERT INTO `transactions` (`customer_id`,`transaction_amount`)
					VALUES('$cid',('$charge'*-1))")) error_log('SMSTavla: '.mysqli_error($db));
			}
			else {
				$dberror = true;
				$error = true;
				error_log('SMSTavla: '.mysqli_error($db));
			}
		}
	}
}

if(isset($error)) {
	echo '<div class="alert alert-danger" role="alert">';
	echo '<h3>Prøv igjen</h3>';
	echo '<ul>';
	if(isset($nocusterr))
		echo '<li>Velg kunde som skal trekkes for meldingene</li>';
	if(isset($invcusterr))
		echo '<li>Velg gyldig kunde som skal trekkes for meldingene</li>';
	if(isset($norecerr))
		echo '<li>Meldingen har ingen mottakere</li>';
	if(isset($chargeerror))
		echo '<li>Ikke tilstrekkelig pengebalanse og/eller overtrekking</li>';
	if(isset($dberror))
		echo '<li>Databasefeil</li>';
	echo '</ul>';
	echo '</div>';
}
if(isset($success))
	echo '<div 
		class="alert alert-success" 
		role="alert">
			Meldingene ble lagt til i køen
		</div>';

?>
<form method="post">
	<div class="form-group">
		<label for="customer">Fra</label>
		<select class="custom-select" id="customer" name="customer"><?php
			foreach($_SESSION['customers'] as $customer) {
				echo '<option value="'.$customer['customer_id'].'">'.htmlspecialchars($customer['customer_name']).'</option>';
			}
		?></select>
	</div>
	
	<div class="form-group">
		<label for="recipients">Mottakere (en per linje)</label>
		<textarea class="form-control" id="recipients" name="recipients" rows="2" required><?= @implode("\n", $_GET['n']) ?></textarea>
	</div>
	
	<div class="form-group">
		<label for="message">Melding</label>
		<textarea class="form-control" id="message" name="message" rows="4" required></textarea>
	</div>
	<button type="submit" class="btn btn-primary mb-2">Send</button>
</form>
<?php
	}
include('templates/bottom.php');
